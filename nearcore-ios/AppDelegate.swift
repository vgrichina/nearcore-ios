//
//  AppDelegate.swift
//  nearcore-ios
//
//  Created by Vladimir Grichina on 12/27/18.
//  Copyright © 2018 NEAR Protocol. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var serviceThread: Thread?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        signal(SIGPIPE, SIG_IGN)

        self.serviceThread = Thread(block: {
            self.runService();
        });
        self.serviceThread?.start();

        return true
    }

    func runService() {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let keystore = ["public_key": "EXGsk7dFpGvb5KWQCWMU2BNFvt9ekfKvWMRLLKo5bt9p",
                        "secret_key": "SjuVT9HuuoCLiMaiKQeZSfKA82SWy7MHG5LfE2UmVBZRvVbpYfhuMdABiARVGi1p1kipzik9TMUediagbHJce4J"]
        
        print(keystore["public_key"]!)
        do {
            let keyStorePath = documentsPath + "/keystore"
            if !FileManager.default.fileExists(atPath: keyStorePath) {
                try FileManager.default.createDirectory(atPath: keyStorePath, withIntermediateDirectories: false)
                try JSONSerialization.data(withJSONObject: keystore)
                    .write(to: .init(fileURLWithPath: keyStorePath + "/" + keystore["public_key"]!))
            }
        } catch {
            print("Unknown error: \(error)");
        }
        
        run_with_config(documentsPath,
                        "alice.near",
                        keystore["public_key"],
                        Bundle.main.resourcePath! + "/default_chain.json",
                        "debug",
                        30333,
                        3030,
                        "/ip4/10.0.1.24/tcp/30333/p2p/QmXiB3jqqn2rpiKU7k1h7NJYeBg8WNSx9DiTRKz9ti2KSK",
                        1);
    }
}

