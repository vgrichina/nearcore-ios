//
//  nearlib.h
//  nearcore-ios
//
//  Created by Vladimir Grichina on 12/27/18.
//  Copyright © 2018 NEAR Protocol. All rights reserved.
//

#ifndef nearlib_h
#define nearlib_h

void run_with_config(const char *base_path_ptr,
                     const char *account_id_ptr,
                     const char *public_key_ptr,
                     const char *chain_spec_path_ptr,
                     const char *log_level_ptr,
                     short p2p_port,
                     short rpc_port,
                     const char *boot_nodes_ptr,
                     int test_network_key_seed_val);

#endif /* nearlib_h */
